package ru.resolutionpoint.routingdemo.client;

import feign.Feign;
import feign.gson.GsonDecoder;
import ru.resolutionpoint.routingdemo.rest.RoutingResponse;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RoutingDemoClient {
    public static final String API_URL = "http://localhost:8080";

    public static void main(String... args) throws IOException {
        RoutingService rsclient = Feign.builder()
                .decoder(new GsonDecoder())
                .target(RoutingService.class, API_URL);

        Map<String, String> queryParamMap = new HashMap<>();
        queryParamMap.put("roleId", "41");
        queryParamMap.put("medicalEntityId", "All");
        queryParamMap.put("newUI", "1");

        RoutingResponse response = rsclient.route(queryParamMap);
        System.out.println(response.getTargetUrl());
    }
}
