package ru.resolutionpoint.routingdemo.client;

import feign.QueryMap;
import feign.RequestLine;
import ru.resolutionpoint.routingdemo.rest.RoutingResponse;

import java.util.Map;

public interface RoutingService {

    @RequestLine("GET /rest/routing")
    RoutingResponse route(@QueryMap Map<String, String> options);
}
