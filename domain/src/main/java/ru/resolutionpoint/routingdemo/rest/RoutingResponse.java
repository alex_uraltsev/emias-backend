package ru.resolutionpoint.routingdemo.rest;

import com.google.gson.Gson;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RoutingResponse implements Serializable {
    private List<BasicNameValuePair> queryParameters;
    private String targetUrl;
    private int status = HttpStatus.SC_OK;
    private Exception exception;

    public RoutingResponse(List<NameValuePair> params, String targetUrl) {
        this.queryParameters = new ArrayList<>();
        for (NameValuePair nameValuePair : params) {
            this.queryParameters.add(new BasicNameValuePair(nameValuePair.getName(), nameValuePair.getValue()));
        }
        this.targetUrl = targetUrl;
    }

    public List<BasicNameValuePair> getQueryParameters() {
        return queryParameters;
    }

    public void setQueryParameters(List<BasicNameValuePair> queryParameters) {
        this.queryParameters = queryParameters;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Exception getException() {
        return exception;
    }

    public void setException(Exception exception) {
        this.exception = exception;
    }

    public String toJson() {
        return new Gson().toJson(this);
    }

    public boolean isOk() {
        return getStatus() == HttpStatus.SC_OK;
    }
}
