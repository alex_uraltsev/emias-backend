package ru.resolutionpoint.routingdemo.repository;

import ru.resolutionpoint.routingdemo.exception.RoutingException;
import ru.resolutionpoint.routingdemo.entity.Rule;

import java.util.Collection;

public interface RuleRepository extends Repository {

    Collection<Rule> all() throws RoutingException;

    void add(Rule taskBase);

    void remove(Rule taskBase);

    void update(Rule rule);

    Rule get(Integer ruleId) throws RoutingException;
}
