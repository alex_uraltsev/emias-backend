package ru.resolutionpoint.routingdemo.repository;

import ru.resolutionpoint.routingdemo.entity.RuleParameter;
import ru.resolutionpoint.routingdemo.exception.RoutingException;

import java.util.Collection;

public interface RuleParameterRepository extends Repository {

    Collection<RuleParameter> all() throws RoutingException;

    void add(RuleParameter parameter);

    void remove(RuleParameter parameter);

    void update(RuleParameter parameter);

    RuleParameter get(Integer parameterId) throws RoutingException;
}
