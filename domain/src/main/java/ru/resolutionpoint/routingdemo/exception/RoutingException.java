package ru.resolutionpoint.routingdemo.exception;

public class RoutingException extends Exception {

    public RoutingException() {
    }

    public RoutingException(RoutingExceptionCode code, String... params) {
        super(code.getCode() + ": " + String.format(code.getMessage(), params));
    }

    public RoutingException(RoutingExceptionCode code, Throwable cause, String... params) {
        super(code.getCode() + ": " + String.format(code.getMessage(), params), cause);
    }

    public RoutingException(String message) {
        super(message);
    }

    public RoutingException(Throwable cause) {
        super(cause);
    }

    public RoutingException(String message, Throwable cause) {
        super(message, cause);
    }
}
