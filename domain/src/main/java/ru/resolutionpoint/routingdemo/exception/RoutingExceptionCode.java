package ru.resolutionpoint.routingdemo.exception;

public enum RoutingExceptionCode {

    NO_RULES("C001", "Не найдено правило маршрутизации"),
    COMMON("C002", "Непридвиденная ошибка"),
    DUPLICATE_PARAMETER("C003", "Обнаружен дубликат параметра: %s"),
    DELETE_NOT_ALLOWED("C004", "Удаление параметра %s недопустимо, пока есть активное правило сформированное этим параметром"),
    PARAMETER_ALREADY_DEFINED("C005", "В правиле уже задан параметр %s."),
    RULES_CONFLICT("C006", "Конфликт правил. %s с таким значением уже задан в одном из правил."),
    UNKNOWN_PARAMETER("C007", "Неизвестный параметр %s."),
    REQUIRED_ATTRIBUTE_NOT_DEFINED("C008", "Не задано обязательное поле %s."),
    PARAMETER_NOT_FOUND("C009", "Параметр с идентификатором %s не найден."),
    UNKNOWN_RULE("C010", "Неизвестное правило %s.");

    private String code;
    private String message;

    RoutingExceptionCode(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
