package ru.resolutionpoint.routingdemo.entity;

public enum ParameterType {
    TEXT,
    BOOL,
    NUMERIC
}
