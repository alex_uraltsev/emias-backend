package ru.resolutionpoint.routingdemo.entity;

import javax.persistence.*;

@Entity
@Table(name = "RULE_PARAMETERS")
public class RuleToRuleParameterLink {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="rule_parameters_id_seq")
    @SequenceGenerator(name="rule_parameters_id_seq", sequenceName="rule_parameters_id_seq", allocationSize=1)
    @Column(name = "ID")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "RULE_ID")
    private Rule rule;

    @ManyToOne
    @JoinColumn(name = "PARAMETER_ID")
    private RuleParameter parameter;

    @Column(name = "PARAM_VALUE")
    private String value;

    @Column(name = "VALUE_CONDITION")
    @Enumerated(EnumType.STRING)
    private ValueCondition valueCondition;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Rule getRule() {
        return rule;
    }

    public void setRule(Rule rule) {
        this.rule = rule;
    }

    public RuleParameter getParameter() {
        return parameter;
    }

    public void setParameter(RuleParameter parameter) {
        this.parameter = parameter;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ValueCondition getValueCondition() {
        return valueCondition;
    }

    public void setValueCondition(ValueCondition valueCondition) {
        this.valueCondition = valueCondition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RuleToRuleParameterLink)) return false;

        RuleToRuleParameterLink that = (RuleToRuleParameterLink) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
