package ru.resolutionpoint.routingdemo.entity;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "RULES")
public class Rule {

    public static final String ID_COLUMN = "ID";
    public static final String NAME_COLUMN = "NAME";
    public static final String TARGET_URL_COLUMN = "TARGET_URL";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="rules_id_seq")
    @SequenceGenerator(name="rules_id_seq", sequenceName="rules_id_seq", allocationSize=1)
    @Column(name = ID_COLUMN)
    private Integer id;

    @Column(name = NAME_COLUMN)
    private String name;

    @Column(name = TARGET_URL_COLUMN)
    private String targetURL;

    @Column
    @OneToMany(mappedBy = "rule", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private List<RuleToRuleParameterLink> parameterLinkList;

    @Transient
    private Integer accuracy = 0;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTargetURL() {
        return targetURL;
    }

    public void setTargetURL(String targetURL) {
        this.targetURL = targetURL;
    }

    public List<RuleToRuleParameterLink> getParameterLinkList() {
        return parameterLinkList;
    }

    public void setParameterLinkList(List<RuleToRuleParameterLink> parameterLinkList) {
        this.parameterLinkList = parameterLinkList;
    }

    public Integer getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Integer accuracy) {
        this.accuracy = accuracy;
    }

    public void clearTransientData() {
        setAccuracy(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Rule)) return false;

        Rule rule = (Rule) o;

        return id.equals(rule.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public static String toJson(Collection<Rule> rules) {
        if (rules != null) {
            List<RuleInfo> ruleInfoList = new ArrayList<>();
            for (Rule rule : rules) {
                ruleInfoList.add(new RuleInfo(rule));
            }
            return new Gson().toJson(ruleInfoList);
        }
        return "";
    }
}
