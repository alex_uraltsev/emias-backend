package ru.resolutionpoint.routingdemo.entity;

public enum ValueCondition {
    EQUALS,
    NOT_EQUALS,
    LESS,
    MORE
}
