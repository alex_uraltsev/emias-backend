package ru.resolutionpoint.routingdemo.entity;

import java.util.HashMap;
import java.util.Map;

public class RuleInfo {

    private String name;
    private String targetURL;
    private Map<String, String> parameterToValues = new HashMap<>();

    public RuleInfo(Rule rule) {
        this.name = rule.getName();
        this.targetURL = rule.getTargetURL();
        for (RuleToRuleParameterLink link : rule.getParameterLinkList()) {
            parameterToValues.put(link.getParameter().getName(), link.getValue());
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTargetURL() {
        return targetURL;
    }

    public void setTargetURL(String targetURL) {
        this.targetURL = targetURL;
    }

    public Map<String, String> getParameterToValues() {
        return parameterToValues;
    }

    public void setParameterToValues(Map<String, String> parameterToValues) {
        this.parameterToValues = parameterToValues;
    }
}
