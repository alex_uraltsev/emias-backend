package ru.resolutionpoint.routingdemo.entity;

import com.google.gson.Gson;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "PARAMETERS")
public class RuleParameter {

    public static final String ID_COLUMN = "ID";
    public static final String NAME_COLUMN = "NAME";
    public static final String TYPE_COLUMN = "TYPE";
    public static final String DESCRIPTION_COLUMN = "DESCRIPTION";
    public static final String RANK_COLUMN = "RANK";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator="parameters_id_seq")
    @SequenceGenerator(name="parameters_id_seq", sequenceName="parameters_id_seq", allocationSize=1)
    @Column(name = ID_COLUMN)
    private Integer id;

    @Column(name = NAME_COLUMN)
    private String name;

    @Column(name = TYPE_COLUMN)
    @Enumerated(EnumType.STRING)
    private ParameterType type;

    @Column(name = DESCRIPTION_COLUMN)
    private String description;

    @Column(name = RANK_COLUMN)
    private Integer rank;

    @Transient
    private String userValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParameterType getType() {
        return type;
    }

    public void setType(ParameterType type) {
        this.type = type;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public String getUserValue() {
        return userValue;
    }

    public void setUserValue(String userValue) {
        this.userValue = userValue;
    }

    public void clearTransientData() {
        setUserValue(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RuleParameter)) return false;

        RuleParameter that = (RuleParameter) o;

        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    public static String toJson(Collection<RuleParameter> ruleParameters) {
        if (ruleParameters != null) {
            return new Gson().toJson(ruleParameters);
        }
        return "";
    }
}
