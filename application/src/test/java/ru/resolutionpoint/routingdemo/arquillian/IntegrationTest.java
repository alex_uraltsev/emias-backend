package ru.resolutionpoint.routingdemo.arquillian;

import org.apache.http.NameValuePair;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.wildfly.swarm.jaxrs.JAXRSArchive;
import ru.resolutionpoint.routingdemo.entity.*;
import ru.resolutionpoint.routingdemo.exception.RoutingException;
import ru.resolutionpoint.routingdemo.repository.*;
import ru.resolutionpoint.routingdemo.rest.RestApplication;
import ru.resolutionpoint.routingdemo.rest.RoutingRSEndpoint;
import ru.resolutionpoint.routingdemo.rest.RoutingResponse;
import ru.resolutionpoint.routingdemo.services.PersistentService;
import ru.resolutionpoint.routingdemo.services.RoutingService;
import ru.resolutionpoint.routingdemo.utils.RoutingUtils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URISyntaxException;
import java.net.URL;

@RunWith(Arquillian.class)
public class IntegrationTest {

    @ArquillianResource
    private URL base;

    @Test
    @RunAsClient
    public void testRestEndpoint() throws URISyntaxException {
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(base.toURI()).path("/rest/routing");
        final Response okResponse = webTarget.
                 queryParam("roleId", "40")
                .queryParam("medicalEntityID", "All")
                .queryParam("newUI", "1")
                .request(MediaType.APPLICATION_JSON)
                .get();
        Assert.assertTrue(okResponse.getStatus() == 200);
        okResponse.close();

        final Response errorResponse = webTarget.
                queryParam("roleId", "40")
                .queryParam("medicalEntityID", "All")
                .queryParam("newUI", "5")
                .request(MediaType.APPLICATION_JSON)
                .get();
        Assert.assertTrue(errorResponse.getStatus() == 500);
    }

    @Deployment(testable = false)
    public static Archive createDeployment() throws Exception {
        return ShrinkWrap
                .create(JAXRSArchive.class)
                .addResource(RestApplication.class)
                .addResource(RoutingRSEndpoint.class)
                .addPackage("ru.resolutionpoint.routingdemo.exception")
                .addPackage("ru.resolutionpoint.routingdemo.repository")
                .addPackage("ru.resolutionpoint.routingdemo.services")
                .addPackage("ru.resolutionpoint.routingdemo.utils")
                .addPackage("ru.resolutionpoint.routingdemo.utils.matcher")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsResource("test-create.sql", "META-INF/sql/create.sql")
                .addAsResource("test-data.sql", "META-INF/sql/data.sql")
                .addAsResource("test-drop.sql", "META-INF/sql/drop.sql")
                .addAllDependencies(true);
    }
}
