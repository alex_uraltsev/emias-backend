package ru.resolutionpoint.routingdemo.junit;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.junit.Assert;
import org.junit.Test;
import ru.resolutionpoint.routingdemo.entity.*;
import ru.resolutionpoint.routingdemo.exception.RoutingException;
import ru.resolutionpoint.routingdemo.exception.RoutingExceptionCode;
import ru.resolutionpoint.routingdemo.utils.RoutingUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class RoutingUtilsTest {

    @Test
    public void testCase() throws Exception {
        List<NameValuePair> nameValuePairList = buildRequestParams("roleId=40&medicalEntityID=All&newUI=1");
        List<RuleParameter> parameters = buildParameters();
        List<Rule> rules = buildRulesCase1(parameters);

        String targetUrl = RoutingUtils.getTargetUrl(nameValuePairList, rules, parameters);
        Assert.assertEquals("/web-attacher/accessPoint/index.api", targetUrl); // сработало частное правило

        nameValuePairList = buildRequestParams("roleId=41&medicalEntityID=All&newUI=1");
        targetUrl = RoutingUtils.getTargetUrl(nameValuePairList, rules, parameters);
        Assert.assertEquals("/web-registrator/accessPoint/index.api", targetUrl); //сработало частное правило

        nameValuePairList = buildRequestParams("roleId=61&medicalEntityID=All&newUI=1");
        targetUrl = RoutingUtils.getTargetUrl(nameValuePairList, rules, parameters);
        Assert.assertEquals("/web-registrator-llo-app/accessPoint/index.api", targetUrl); //сработало общее правило All

    }

    @Test
    public void testParseString() throws Exception {
        Map<String, String> result = RoutingUtils.parseParametersFromString("roleId=1,2,3&medicalEntityID=All&newUI=1");
        Assert.assertTrue(result.size() == 3);
        Assert.assertTrue("1,2,3".equals(result.get("roleId")));
        Assert.assertTrue("All".equals(result.get("medicalEntityID")));
        Assert.assertTrue("1".equals(result.get("newUI")));

        try {
            result = RoutingUtils.parseParametersFromString("roleId=1,2,3&medicalEntityID=All&newUI=1&newUI=2");
        } catch (RoutingException e) {
            Assert.assertTrue(e.getMessage().contains(RoutingExceptionCode.PARAMETER_ALREADY_DEFINED.getCode()));
        }
    }


    private List<NameValuePair> buildRequestParams(String value) throws Exception {
        Map<String, String> map = RoutingUtils.parseParametersFromString(value);
        List<NameValuePair> result = new ArrayList<>();
        for (String key : map.keySet()) {
            result.add(new BasicNameValuePair(key, map.get(key)));
        }
        return result;
    }

    private List<Rule> buildRulesCase1(List<RuleParameter> ruleParameters) {
        List<Rule> rules = new ArrayList<>();

        Rule rule1 = buildRule("Rule 1", "/web/accessPoint/index.api", "1", "All", "1", ruleParameters);
        rules.add(rule1);

        Rule rule2 = buildRule("Rule 2", "/web-department-manager/accessPoint/index.api", "27", "All", "1", ruleParameters);
        rules.add(rule2);

        Rule rule3 = buildRule("Rule 3", "/web-specialist-llo-app/accessPoint/index.api", "101", "All", "1", ruleParameters);
        rules.add(rule3);

        Rule rule4 = buildRule("Rule 4", "/web-specialist-llo-app/accessPoint/index.api", "100", "All", "1", ruleParameters);
        rules.add(rule4);

        Rule rule5 = buildRule("Rule 5", "/web-admin-llo-app/accessPoint/index.api", "21", "All", "1", ruleParameters);
        rules.add(rule5);

        Rule rule6 = buildRule("Rule 6", "/web-registrator-llo-app/accessPoint/index.api", "All", "All", "1", ruleParameters);
        rules.add(rule6);

        Rule rule7 = buildRule("Rule 7", "/web-registrator/accessPoint/index.api", "5,20,7,41", "All", "1", ruleParameters);
        rules.add(rule7);

        Rule rule8 = buildRule("Rule 8", "/web-attacher/accessPoint/index.api", "40", "All", "1", ruleParameters);
        rules.add(rule8);

        return rules;
    }

    private Rule buildRule(String name, String targetURL, String param1, String param2, String param3, List<RuleParameter> parameters) {
        Rule rule = new Rule();
        rule.setId(1);
        rule.setTargetURL(targetURL);
        rule.setName(name);
        List<RuleToRuleParameterLink> list = new ArrayList<>();
        RuleToRuleParameterLink link1 = new RuleToRuleParameterLink();
        link1.setRule(rule);
        link1.setParameter(parameters.get(0));
        link1.setValue(param1);
        link1.setValueCondition(ValueCondition.EQUALS);
        list.add(link1);

        RuleToRuleParameterLink link2 = new RuleToRuleParameterLink();
        link2.setRule(rule);
        link2.setParameter(parameters.get(1));
        link2.setValue(param2);
        link2.setValueCondition(ValueCondition.EQUALS);
        list.add(link2);

        RuleToRuleParameterLink link3 = new RuleToRuleParameterLink();
        link3.setRule(rule);
        link3.setParameter(parameters.get(2));
        link3.setValue(param3);
        link3.setValueCondition(ValueCondition.EQUALS);
        list.add(link3);

        rule.setParameterLinkList(list);
        return rule;
    }

    private List<RuleParameter> buildParameters() {
        List<RuleParameter> parameters = new ArrayList<>();

        RuleParameter ruleParameter1 = new RuleParameter();
        ruleParameter1.setId(1);
        ruleParameter1.setName("roleID");
        ruleParameter1.setRank(2);
        ruleParameter1.setType(ParameterType.NUMERIC);

        parameters.add(ruleParameter1);

        RuleParameter ruleParameter2 = new RuleParameter();
        ruleParameter2.setId(2);
        ruleParameter2.setName("medicalEntityID");
        ruleParameter2.setRank(1);
        ruleParameter2.setType(ParameterType.TEXT);

        parameters.add(ruleParameter2);

        RuleParameter ruleParameter3 = new RuleParameter();
        ruleParameter3.setId(3);
        ruleParameter3.setName("newUI");
        ruleParameter3.setRank(3);
        ruleParameter3.setType(ParameterType.BOOL);

        parameters.add(ruleParameter3);

        return parameters;
    }
}
