package ru.resolutionpoint.routingdemo.rest;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.resolutionpoint.routingdemo.services.RoutingService;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import java.util.List;

@Path("/routing")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@RequestScoped
public class RoutingRSEndpoint {

    private static Logger logger = LoggerFactory.getLogger(RoutingRSEndpoint.class.getName());

    @Inject
    private RoutingService routingService;

    public RoutingRSEndpoint() {
    }

    @GET
    public Response route(@Context UriInfo uriInfo) {
        List<NameValuePair> params = URLEncodedUtils.parse(uriInfo.getRequestUri(), "UTF-8");
        for (NameValuePair param : params) {
            logger.info(param.getName() + " : " + param.getValue());
        }

        RoutingResponse response = routingService.getTargetUrl(params);

        logger.info("Target URL - " + response.getTargetUrl());
        if (response.isOk()) {
            return Response.ok(response.toJson(), MediaType.APPLICATION_JSON).build();
        } else {
            return Response.status(response.getStatus()).entity("EXCEPTION: " + response.getException().getMessage()).build();
        }
    }
}
