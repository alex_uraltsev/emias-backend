package ru.resolutionpoint.routingdemo.utils.matcher;

public class BooleanMatcher implements Matcher {

    @Override
    public boolean isMatch(String userValue, String parameterValue) {
        return ALL.equalsIgnoreCase(parameterValue) || convertToBoolean(userValue) == convertToBoolean(parameterValue);
    }

    private boolean convertToBoolean(String value) {
        boolean returnValue = false;
        if ("1".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value) ||
                "true".equalsIgnoreCase(value) || "on".equalsIgnoreCase(value))
            returnValue = true;
        return returnValue;
    }
}
