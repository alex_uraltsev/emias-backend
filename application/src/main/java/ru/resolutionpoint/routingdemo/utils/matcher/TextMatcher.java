package ru.resolutionpoint.routingdemo.utils.matcher;

public class TextMatcher implements Matcher {
    @Override
    public boolean isMatch(String userValue, String parameterValue) {
        return ALL.equalsIgnoreCase(parameterValue) || parameterValue.equalsIgnoreCase(userValue);
    }
}
