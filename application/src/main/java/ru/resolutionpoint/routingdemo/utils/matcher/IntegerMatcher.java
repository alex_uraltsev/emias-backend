package ru.resolutionpoint.routingdemo.utils.matcher;

public class IntegerMatcher implements Matcher {

    @Override
    public boolean isMatch(String userValue, String parameterValueStr) {
        if (ALL.equalsIgnoreCase(parameterValueStr)) {
            return true;
        }

        String[] parameterValues = parameterValueStr.split(",");
        boolean matched = false;
        for (String parameterValue : parameterValues) {
            if (Integer.valueOf(userValue).equals(Integer.valueOf(parameterValue))) {
                matched = true;
                break;
            }
        }
        return matched;
    }
}
