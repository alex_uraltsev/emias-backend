package ru.resolutionpoint.routingdemo.utils;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import ru.resolutionpoint.routingdemo.entity.*;
import ru.resolutionpoint.routingdemo.exception.RoutingException;
import ru.resolutionpoint.routingdemo.exception.RoutingExceptionCode;
import ru.resolutionpoint.routingdemo.utils.matcher.*;

import java.util.*;

public class RoutingUtils {

    private static final Map<ParameterType, Matcher> equality_matchers = new HashMap<>();
    private static final Map<ParameterType, Matcher> less_matchers = new HashMap<>();
    private static final Map<ParameterType, Matcher> more_matchers = new HashMap<>();

    static {
        equality_matchers.put(ParameterType.TEXT, new TextMatcher());
        equality_matchers.put(ParameterType.NUMERIC, new IntegerMatcher());
        equality_matchers.put(ParameterType.BOOL, new BooleanMatcher());

        less_matchers.put(ParameterType.NUMERIC, new IntegerLessMatcher());
        more_matchers.put(ParameterType.NUMERIC, new IntegerMoreMatcher());
    }

    private static final String PARAMETER_DELIMITER = "&";
    private static final String VALUE_DELIMITER = "=";

    public static String getTargetUrl(List<NameValuePair> requestParams, Collection<Rule> rules,
                                      Collection<RuleParameter> ruleParameters) throws RoutingException {
        // обработка входных параметров и сортировка по рангу
        List<RuleParameter> requestedParameters = new ArrayList<>();
        clearTransientData(rules, ruleParameters);

        for (NameValuePair requestParam : requestParams) {
            RuleParameter ruleParameter = findParameter(requestParam.getName(), ruleParameters);
            if (ruleParameter == null) {
                throw new RoutingException(RoutingExceptionCode.UNKNOWN_PARAMETER, requestParam.getName());
            }
            ruleParameter.setUserValue(requestParam.getValue());
            requestedParameters.add(ruleParameter);
        }
        requestedParameters.sort(Comparator.comparingInt(RuleParameter::getRank));

        List<Rule> availableRules = new ArrayList<Rule>();
        for (Rule rule : rules) {
            boolean matched = true;
            for (RuleParameter parameter : requestedParameters) {
                //ищем условие для этого параметра
                RuleToRuleParameterLink ruleToRuleParameterLink = findRuleToParameterLink(parameter, rule.getParameterLinkList());
                if (ruleToRuleParameterLink != null) {
                    matched = valueMatched(parameter.getUserValue(), ruleToRuleParameterLink.getValue(),
                            parameter.getType(), ruleToRuleParameterLink.getValueCondition());
                    if (!matched) {
                        break; // по одному из условий не прошли - вылетаем из цикла
                    }
                    if (!Matcher.ALL.equalsIgnoreCase(ruleToRuleParameterLink.getValue())) {
                        rule.setAccuracy(rule.getAccuracy() + 10);
                    }
                }
            }
            if (matched) { //правило сработало по всем условиям - добавляем в список доступных правил
                availableRules.add(rule);
            }
        }

        return getMoreCorrectRule(availableRules);
    }

    private static String getMoreCorrectRule(List<Rule> availableRules) throws RoutingException {
        if (availableRules.size() > 0) {
            if (availableRules.size() > 1) {
                availableRules.sort(Comparator.comparingInt(Rule::getAccuracy).reversed());
                return availableRules.get(0).getTargetURL();
            } else {
                return availableRules.get(0).getTargetURL();
            }
        }
        throw new RoutingException(RoutingExceptionCode.NO_RULES);
    }

    public static RuleParameter findParameter(String name, Collection<RuleParameter> ruleParameterCollection) {
        for (RuleParameter ruleParameter : ruleParameterCollection) {
            if (ruleParameter.getName().equalsIgnoreCase(name)) {
                return ruleParameter;
            }
        }
        return null;
    }

    public static RuleToRuleParameterLink findRuleToParameterLink(RuleParameter ruleParameter, List<RuleToRuleParameterLink> list) {
        for (RuleToRuleParameterLink ruleToRuleParameterLink : list) {
            if (ruleToRuleParameterLink.getParameter().getId().equals(ruleParameter.getId())) {
                return ruleToRuleParameterLink;
            }
        }
        return null;
    }

    private static boolean valueMatched(String userValue, String parameterValue, ParameterType parameterType, ValueCondition parameterCondition) {
        switch (parameterCondition) {
            case EQUALS:
                return checkEqualsCondition(userValue, parameterValue, parameterType);
            case NOT_EQUALS:
                return !checkEqualsCondition(userValue, parameterValue, parameterType);
            case LESS:
                return checkLessCondition(userValue, parameterValue, parameterType);
            case MORE:
                return checkMoreCondition(userValue, parameterValue, parameterType);
        }
        return false;
    }

    private static boolean checkEqualsCondition(String value, String parameterValue, ParameterType parameterType) {
        Matcher matcher =  equality_matchers.get(parameterType);
        return matcher != null && matcher.isMatch(value, parameterValue);
    }

    private static boolean checkMoreCondition(String value, String parameterValue, ParameterType parameterType) {
        Matcher matcher = more_matchers.get(parameterType);
        return matcher != null && matcher.isMatch(value, parameterValue);
    }

    private static boolean checkLessCondition(String value, String parameterValue, ParameterType parameterType) {
        Matcher matcher =  less_matchers.get(parameterType);
        return matcher != null && matcher.isMatch(value, parameterValue);
    }

    public static boolean isValueUndefined(String value) {
        return value == null || value.isEmpty();
    }

    public static Map<String, String> parseParametersFromString(String parametersStr) throws RoutingException {
        Map<String, String> map = new HashMap<>();
        String[] parameters = parametersStr.split(PARAMETER_DELIMITER);
        for (String parameter : parameters) {
            String[] values = parameter.split(VALUE_DELIMITER);
            if (values.length == 2) {
                if (map.containsKey(values[0])) {
                    throw new RoutingException(RoutingExceptionCode.PARAMETER_ALREADY_DEFINED, values[0]);
                }
                map.put(values[0], values[1]);
            } else {
                throw new IllegalArgumentException();
            }
        }
        return map;
    }

    private static void clearTransientData(Collection<Rule> rules, Collection<RuleParameter> ruleParameters) {
        for (Rule rule : rules) {
            rule.clearTransientData();
        }
        for (RuleParameter ruleParameter : ruleParameters) {
            ruleParameter.clearTransientData();
        }
    }
}
