package ru.resolutionpoint.routingdemo.utils.matcher;

public interface Matcher {
    String ALL = "All";

    boolean isMatch(String userValue, String parameterValue);
}
