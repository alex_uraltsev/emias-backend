package ru.resolutionpoint.routingdemo.utils.matcher;

public class IntegerMoreMatcher implements Matcher {
    @Override
    public boolean isMatch(String userValue, String parameterValue) {
        return Integer.valueOf(userValue).compareTo(Integer.valueOf(parameterValue)) > 0;
    }
}
