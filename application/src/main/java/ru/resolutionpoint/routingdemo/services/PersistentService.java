package ru.resolutionpoint.routingdemo.services;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Stateless
public class PersistentService<T> {

    @PersistenceContext(name = "routingDemoPU", unitName = "routingDemoPU")
    EntityManager em;

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void persist(T object) {
        em.persist(object);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void remove(T object) {
        em.remove(object);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void merge(T object) {
        em.merge(object);
    }

    public T getById(Class<T> clazz, Integer id)  {
        return em.find(clazz, id);
    }

    public List<T> loadAll(Class<T> clazz) {
        return em.createQuery("from " +  clazz.getName()).getResultList();
    }

}
