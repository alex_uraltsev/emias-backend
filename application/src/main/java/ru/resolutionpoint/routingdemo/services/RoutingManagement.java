package ru.resolutionpoint.routingdemo.services;

import com.udojava.jmx.wrapper.*;
import ru.resolutionpoint.routingdemo.entity.ParameterType;
import ru.resolutionpoint.routingdemo.entity.Rule;
import ru.resolutionpoint.routingdemo.entity.RuleParameter;
import ru.resolutionpoint.routingdemo.exception.RoutingException;
import ru.resolutionpoint.routingdemo.utils.RoutingUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import javax.management.*;
import java.lang.management.ManagementFactory;
import java.util.Collection;

@Startup
@Singleton
@JMXBean(description = "Routing demo JMX view")
public class RoutingManagement implements RoutingManagementMBean {

    @Inject
    ParameterService parameterService;

    @Inject
    RulesService rulesService;

    private MBeanServer platformMBeanServer;
    private ObjectName objectName = null;

    @PostConstruct
    public void registerInJMX() {
        try {
            JMXBeanWrapper wrappedBean = new JMXBeanWrapper(this);
            objectName = new ObjectName("RoutingManagement:type=" + this.getClass().getName());
            platformMBeanServer = ManagementFactory.getPlatformMBeanServer();
            platformMBeanServer.registerMBean(wrappedBean, objectName);
        } catch (Exception e) {
            throw new IllegalStateException("Problem during registration of RoutingManagement into JMX:" + e);
        }
    }

    @PreDestroy
    public void unregisterFromJMX() {
        try {
            platformMBeanServer.unregisterMBean(this.objectName);
        } catch (Exception e) {
            throw new IllegalStateException("Problem during unregistration of RoutingManagement into JMX:" + e);
        }
    }

    @Override
    @JMXBeanAttribute
    public String getRules() throws RoutingException {
        Collection<Rule> rules = rulesService.getRules();
        return Rule.toJson(rules);
    }

    @Override
    @JMXBeanAttribute
    public String getParameters() throws RoutingException {
        Collection<RuleParameter> ruleParameters = parameterService.getParameters();
        return RuleParameter.toJson(ruleParameters);
    }

    @Override
    @JMXBeanOperation(name = "Add Parameter", description = "Add new parameter to the system.")
    public void addParameter(@JMXBeanParameter(name = "Name", description = "Name of parameter (required)") String name,
                             @JMXBeanParameter(name = "Type", description = "Type of parameter (required) - TEXT, NUMERIC or BOOL") String type,
                             @JMXBeanParameter(name = "Description", description = "Parameter description") String description,
                             @JMXBeanParameter(name = "Rank", description = "Rank (numeric value)") String rank) throws RoutingException {
        RuleParameter ruleParameter = new RuleParameter();
        ruleParameter.setName(name);
        ruleParameter.setType(RoutingUtils.isValueUndefined(type) ? null : ParameterType.valueOf(type));
        ruleParameter.setDescription(description);
        ruleParameter.setRank(RoutingUtils.isValueUndefined(rank) ? null : Integer.valueOf(rank));
        parameterService.addParameter(ruleParameter);
    }

    @Override
    @JMXBeanOperation(name = "Remove Parameter", description = "Remove existing parameter by id")
    public void removeParameter(@JMXBeanParameter(name = "ID", description = "Parameter Identificator (numeric value)") String id) throws RoutingException {
        parameterService.removeParameter(Integer.valueOf(id));
    }

    @Override
    @JMXBeanOperation(name = "Modify Parameter", description = "Modify existing parameter by id. Need to specify ID of parameter, columnName(NAME,TYPE,DESCRIPTION,RANK) and new value")
    public void modifyParameter(@JMXBeanParameter(name = "ID", description = "Parameter Identificator (numeric value)") String id,
                                @JMXBeanParameter(name = "ColumnName", description = "columnName: NAME, TYPE, DESCRIPTION, RANK") String columnName,
                                @JMXBeanParameter(name = "Value", description = "New value for specified column") String value) throws RoutingException {
        parameterService.modifyParameter(Integer.valueOf(id), columnName, value);
    }

    @Override
    @JMXBeanOperation(name = "Add Rule", description = "Add rule without parameters. Call setupRuleParameters for defining parameter values for created rule.")
    public void addRule(@JMXBeanParameter(name = "Name", description = "Name of rule (required)") String name,
                        @JMXBeanParameter(name = "TargetURL", description = "Target URL (required)") String targetUrl) throws RoutingException {
        Rule rule = new Rule();
        rule.setName(name);
        rule.setTargetURL(targetUrl);
        rulesService.addRule(rule);
    }

    @Override
    @JMXBeanOperation(name = "Remove Rule", description = "Remove existing rule by id")
    public void deleteRule(@JMXBeanParameter(name = "ID", description = "Rule Identificator (numeric value)") String id) throws RoutingException {
        rulesService.removeRule(Integer.valueOf(id));
    }

    @Override
    @JMXBeanOperation(name = "Modify Rule", description = "Modify existing rule by id. Need to specify ID of rule, columnName(NAME,TARGET_URL) and new value")
    public void modifyRule(@JMXBeanParameter(name = "ID", description = "Parameter Identificator (numeric value)") String id,
                           @JMXBeanParameter(name = "ColumnName", description = "columnName: NAME, TARGET_URL") String columnName,
                           @JMXBeanParameter(name = "Value", description = "New value for specified column") String value) throws RoutingException {
        rulesService.modifyRule(Integer.valueOf(id), columnName, value);
    }

    @Override
    @JMXBeanOperation(name = "Setup Rule Parameters", description = "Setup rule conditions by id. Need to specify ID of rule and conditions in special format")
    public void setupRuleParameters(@JMXBeanParameter(name = "ID", description = "Parameter Identificator (numeric value)") String id,
                                    @JMXBeanParameter(name = "Value", description = "Conditions separated by '&' (name and value separated by '='). Example: roleId=1,2,3&medicalEntityID=All&newUI=1") String ruleParametersStr) throws RoutingException {
        rulesService.setupRuleParameters(Integer.valueOf(id), ruleParametersStr);
    }

    @Override
    @JMXBeanOperation(name = "Export parameters to CSV", description = "Export list of all parameters to CSV (local machine)")
    public void exportParametersToCsv(@JMXBeanParameter(name = "FilePath", description = "Path to file on local machine") String filePath) throws Exception {
        parameterService.exportToCsv(filePath);
    }

    @Override
    @JMXBeanOperation(name = "Export rules to CSV", description = "Export list of all rules to CSV (local machine)")
    public void exportRulesToCsv(@JMXBeanParameter(name = "FilePath", description = "Path to file on local machine") String filePath) throws Exception {
        rulesService.exportToCsv(filePath);
    }

}
