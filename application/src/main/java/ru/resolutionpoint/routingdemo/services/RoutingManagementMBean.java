package ru.resolutionpoint.routingdemo.services;

import ru.resolutionpoint.routingdemo.exception.RoutingException;

public interface RoutingManagementMBean {

    String getRules() throws RoutingException;

    String getParameters() throws RoutingException;

    void addParameter(String name, String type, String description, String rank) throws RoutingException;

    void removeParameter(String id) throws RoutingException;

    void modifyParameter(String id, String columnName, String value) throws RoutingException;

    void addRule(String name, String targetUrl) throws RoutingException;

    void deleteRule(String id) throws RoutingException;

    void modifyRule(String id, String columnName, String value) throws RoutingException;

    void setupRuleParameters(String id, String ruleParametersStr) throws RoutingException;

    void exportParametersToCsv(String filePath) throws Exception;

    void exportRulesToCsv(String filePath) throws Exception;
}
