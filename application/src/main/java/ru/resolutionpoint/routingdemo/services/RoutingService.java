package ru.resolutionpoint.routingdemo.services;

import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.resolutionpoint.routingdemo.repository.RuleParameterRepository;
import ru.resolutionpoint.routingdemo.repository.RuleRepository;
import ru.resolutionpoint.routingdemo.rest.RoutingResponse;
import ru.resolutionpoint.routingdemo.utils.RoutingUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class RoutingService {
    private static Logger logger = LoggerFactory.getLogger(RoutingService.class.getName());

    @EJB
    private RuleRepository ruleRepository;

    @EJB
    private RuleParameterRepository ruleParameterRepository;

    public RoutingResponse getTargetUrl(List<NameValuePair> params) {
        RoutingResponse response = new RoutingResponse(params, null);
        try {
            response.setTargetUrl(RoutingUtils.getTargetUrl(params, ruleRepository.all(), ruleParameterRepository.all()));
        } catch (Exception e) {
            response.setStatus(HttpStatus.SC_INTERNAL_SERVER_ERROR);
            response.setException(e);
            logger.error(e.getMessage(), e);
        }
        return response;
    }
}
