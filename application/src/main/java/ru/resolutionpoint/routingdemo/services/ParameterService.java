package ru.resolutionpoint.routingdemo.services;

import au.com.bytecode.opencsv.CSVWriter;
import ru.resolutionpoint.routingdemo.entity.ParameterType;
import ru.resolutionpoint.routingdemo.exception.RoutingException;
import ru.resolutionpoint.routingdemo.exception.RoutingExceptionCode;
import ru.resolutionpoint.routingdemo.entity.Rule;
import ru.resolutionpoint.routingdemo.entity.RuleParameter;
import ru.resolutionpoint.routingdemo.entity.RuleToRuleParameterLink;
import ru.resolutionpoint.routingdemo.repository.RuleParameterRepository;
import ru.resolutionpoint.routingdemo.repository.RuleRepository;
import ru.resolutionpoint.routingdemo.utils.RoutingUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.FileWriter;
import java.util.Collection;

@Stateless
public class ParameterService {

    @EJB
    private RuleParameterRepository parameterRepository;

    @EJB
    private RuleRepository ruleRepository;


    public Collection<RuleParameter> getParameters() throws RoutingException {
        return parameterRepository.all();
    }

    public void addParameter(RuleParameter ruleParameter) throws RoutingException {
        validate(ruleParameter);

        Collection<RuleParameter> existingParameters = getParameters();
        for (RuleParameter existingParameter : existingParameters) {
            if (existingParameter.getName().equalsIgnoreCase(ruleParameter.getName())) {
                throw new RoutingException(RoutingExceptionCode.DUPLICATE_PARAMETER, existingParameter.getName());
            }
        }
        parameterRepository.add(ruleParameter);
    }

    public void removeParameter(Integer id) throws RoutingException {
        if (id == null) {
            throw new RoutingException(RoutingExceptionCode.COMMON);
        }

        RuleParameter ruleParameter = parameterRepository.get(id);

        if (ruleParameter == null) {
            throw new RoutingException(RoutingExceptionCode.PARAMETER_NOT_FOUND, id.toString());
        }
        String parameterName = ruleParameter.getName();

        Collection<Rule> existingRules = ruleRepository.all();
        for (Rule rule : existingRules) {
            for (RuleToRuleParameterLink link : rule.getParameterLinkList()) {
                if (link.getParameter().getId().equals(id)) {
                    throw new RoutingException(RoutingExceptionCode.DELETE_NOT_ALLOWED, parameterName);
                }
            }
        }
        parameterRepository.remove(ruleParameter);
    }

    public void modifyParameter(Integer id, String columnName, String value) throws RoutingException{
        if (id == null || RoutingUtils.isValueUndefined(columnName)) {
            throw new RoutingException(RoutingExceptionCode.COMMON);
        }

        RuleParameter ruleParameter = parameterRepository.get(id);
        if (ruleParameter == null) {
            throw new RoutingException(RoutingExceptionCode.PARAMETER_NOT_FOUND, id.toString());
        }

        switch (columnName) {
            case RuleParameter.NAME_COLUMN : ruleParameter.setName(value);
            case RuleParameter.TYPE_COLUMN :
                if (RoutingUtils.isValueUndefined(value)) {
                    throw new RoutingException(RoutingExceptionCode.COMMON);
                }
                ruleParameter.setType(ParameterType.valueOf(value));
            case RuleParameter.DESCRIPTION_COLUMN : ruleParameter.setDescription(value);
            case RuleParameter.RANK_COLUMN : ruleParameter.setRank(RoutingUtils.isValueUndefined(value) ? null : Integer.valueOf(value));
        }

        parameterRepository.update(ruleParameter);
    }

    public void exportToCsv(String filePath) throws Exception {
        CSVWriter writer = new CSVWriter(new FileWriter(filePath), ';');

        Collection<RuleParameter> ruleParameters = parameterRepository.all();
        writer.writeNext(new String[] {RuleParameter.ID_COLUMN, RuleParameter.NAME_COLUMN, RuleParameter.TYPE_COLUMN, RuleParameter.DESCRIPTION_COLUMN, RuleParameter.RANK_COLUMN});
        for (RuleParameter ruleParameter : ruleParameters) {
            String[] record = new String[]{ruleParameter.getId().toString(), ruleParameter.getName(), ruleParameter.getType().name(), ruleParameter.getDescription(),
                    ruleParameter.getRank() == null ? "" : ruleParameter.getRank().toString()};
            writer.writeNext(record);
        }
        writer.close();
    }

    private void validate(RuleParameter parameter) throws RoutingException {
        if (RoutingUtils.isValueUndefined(parameter.getName())) {
            throw new RoutingException(RoutingExceptionCode.REQUIRED_ATTRIBUTE_NOT_DEFINED, "Name");
        }
        if (parameter.getType() == null) {
            throw new RoutingException(RoutingExceptionCode.REQUIRED_ATTRIBUTE_NOT_DEFINED, "Type");
        }
    }
}
