package ru.resolutionpoint.routingdemo.services;

import au.com.bytecode.opencsv.CSVWriter;
import ru.resolutionpoint.routingdemo.entity.Rule;
import ru.resolutionpoint.routingdemo.entity.RuleParameter;
import ru.resolutionpoint.routingdemo.entity.RuleToRuleParameterLink;
import ru.resolutionpoint.routingdemo.entity.ValueCondition;
import ru.resolutionpoint.routingdemo.exception.RoutingException;
import ru.resolutionpoint.routingdemo.exception.RoutingExceptionCode;
import ru.resolutionpoint.routingdemo.repository.RuleParameterRepository;
import ru.resolutionpoint.routingdemo.repository.RuleRepository;
import ru.resolutionpoint.routingdemo.utils.RoutingUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import java.io.FileWriter;
import java.util.*;

@Stateless
public class RulesService {

    @EJB
    private RuleRepository ruleRepository;

    @EJB
    private RuleParameterRepository ruleParameterRepository;

    public Collection<Rule> getRules() throws RoutingException {
        return ruleRepository.all();
    }

    public void addRule(Rule rule) throws RoutingException {
        validate(rule);
        ruleRepository.add(rule);
    }

    public void removeRule(Integer id) throws RoutingException {
        if (id == null) {
            throw new RoutingException(RoutingExceptionCode.COMMON);
        }

        Rule rule = ruleRepository.get(id);

        if (rule == null) {
            throw new RoutingException(RoutingExceptionCode.UNKNOWN_RULE, id.toString());
        }
        ruleRepository.remove(rule);
    }

    public void modifyRule(Integer id, String columnName, String value) throws RoutingException {
        if (id == null || RoutingUtils.isValueUndefined(columnName)) {
            throw new RoutingException(RoutingExceptionCode.COMMON);
        }

        Rule rule = ruleRepository.get(id);
        if (rule == null) {
            throw new RoutingException(RoutingExceptionCode.UNKNOWN_RULE, id.toString());
        }

        switch (columnName) {
            case Rule.NAME_COLUMN:
                rule.setName(value);
            case Rule.TARGET_URL_COLUMN:
                rule.setTargetURL(value);
        }

        ruleRepository.update(rule);
    }

    public void setupRuleParameters(Integer id, String ruleParametersStr) throws RoutingException {
        if (id == null || RoutingUtils.isValueUndefined(ruleParametersStr)) {
            throw new RoutingException(RoutingExceptionCode.COMMON);
        }

        Rule rule = ruleRepository.get(id);
        if (rule == null) {
            throw new RoutingException(RoutingExceptionCode.UNKNOWN_RULE, id.toString());
        }

        Map<String, String> map = RoutingUtils.parseParametersFromString(ruleParametersStr);
        List<RuleToRuleParameterLink> list = new ArrayList<>();

        Collection<RuleParameter> ruleParameters = ruleParameterRepository.all();
        Collection<Rule> otherRules = ruleRepository.all();

        for (String parameterName : map.keySet()) {
            RuleParameter ruleParameter = RoutingUtils.findParameter(parameterName, ruleParameters);
            if (ruleParameter != null) {
                String value = map.get(parameterName);
                RuleToRuleParameterLink link = new RuleToRuleParameterLink();
                link.setRule(rule);
                if (value.startsWith("!")) {
                    link.setValueCondition(ValueCondition.NOT_EQUALS);
                    value = value.replaceFirst("!", "");
                } else if (value.startsWith(">")) {
                    link.setValueCondition(ValueCondition.MORE);
                    value = value.replaceFirst(">", "");
                } else if (value.startsWith("<")) {
                    link.setValueCondition(ValueCondition.LESS);
                    value = value.replaceFirst("<", "");
                } else {
                    link.setValueCondition(ValueCondition.EQUALS);
                }
                link.setParameter(ruleParameter);
                link.setValue(value);

                checkRuleConflict(rule, link, otherRules);
                list.add(link);
            } else {
                throw new RoutingException(RoutingExceptionCode.UNKNOWN_PARAMETER, parameterName);
            }
        }
        rule.setParameterLinkList(list);

        ruleRepository.update(rule);
    }

    public void exportToCsv(String filePath) throws Exception {
        CSVWriter writer = new CSVWriter(new FileWriter(filePath), ';');

        List<Rule> rules = new ArrayList<>(ruleRepository.all());
        Collection<RuleParameter> parameters = new ArrayList<>(ruleParameterRepository.all());

        String[] headerColumns = new String[3 + parameters.size()];
        headerColumns[0] = Rule.ID_COLUMN;
        headerColumns[1] = Rule.NAME_COLUMN;
        int i = 2;
        for (RuleParameter ruleParameter : parameters) {
            headerColumns[i] = ruleParameter.getName();
            i++;
        }
        headerColumns[i] = Rule.TARGET_URL_COLUMN;

        writer.writeNext(headerColumns);

        rules.sort(Comparator.comparingInt(Rule::getId));
        for (Rule rule : rules) {
            String[] valueColumns = new String[3 + parameters.size()];
            valueColumns[0] = rule.getId().toString();
            valueColumns[1] = rule.getName();
            i = 2;
            for (RuleParameter ruleParameter : parameters) {
                RuleToRuleParameterLink link = RoutingUtils.findRuleToParameterLink(ruleParameter, rule.getParameterLinkList());
                valueColumns[i] = link == null || link.getValue() == null ? "" : link.getValue();
                i++;
            }
            valueColumns[i] = rule.getTargetURL();
            writer.writeNext(valueColumns);
        }
        writer.close();
    }

    private void validate(Rule rule) throws RoutingException {
        if (RoutingUtils.isValueUndefined(rule.getName())) {
            throw new RoutingException(RoutingExceptionCode.REQUIRED_ATTRIBUTE_NOT_DEFINED, "Name");
        }
        if (RoutingUtils.isValueUndefined(rule.getTargetURL())) {
            throw new RoutingException(RoutingExceptionCode.REQUIRED_ATTRIBUTE_NOT_DEFINED, "TargetURL");
        }
    }

    private void checkRuleConflict(Rule rule, RuleToRuleParameterLink link, Collection<Rule> allRules) throws RoutingException {
        for (Rule r : allRules) {
           if (!r.getId().equals(rule.getId())) {
               for (RuleToRuleParameterLink rlink : r.getParameterLinkList()) {
                   if (rlink.getParameter().equals(link.getParameter()) && rlink.getValue().equalsIgnoreCase(link.getValue()) && rlink.getValueCondition().equals(link.getValueCondition())) {
                       throw new RoutingException(RoutingExceptionCode.RULES_CONFLICT, link.getParameter().getName());
                   }
               }
           }
        }
    }
}
