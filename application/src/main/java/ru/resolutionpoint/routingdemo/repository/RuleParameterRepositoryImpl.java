package ru.resolutionpoint.routingdemo.repository;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import ru.resolutionpoint.routingdemo.entity.RuleParameter;
import ru.resolutionpoint.routingdemo.exception.RoutingException;
import ru.resolutionpoint.routingdemo.exception.RoutingExceptionCode;
import ru.resolutionpoint.routingdemo.services.PersistentService;

import javax.ejb.EJB;
import javax.ejb.Singleton;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Singleton
public class RuleParameterRepositoryImpl extends BaseRepository<RuleParameter> implements RuleParameterRepository {

    @EJB
    PersistentService<RuleParameter> persistentService;

    private LoadingCache<Integer, RuleParameter> parameterLoadingCache =
            CacheBuilder.newBuilder().build(new CacheLoader<Integer, RuleParameter>() { // build the cacheloader
                @Override
                public RuleParameter load(Integer ruleParameterId) throws Exception {
                    //make the expensive call
                    return getByIdFromDatabase(RuleParameter.class, ruleParameterId);
                }
            });

    @Override
    public Collection<RuleParameter> all() throws RoutingException {
        if (parameterLoadingCache.size() > 0) {
            return parameterLoadingCache.asMap().values();
        }

        List<RuleParameter> ruleParameters = getAllFromDatabase(RuleParameter.class);
        Map<Integer, RuleParameter> map = new HashMap<>();
        for (RuleParameter ruleParameter : ruleParameters) {
            map.put(ruleParameter.getId(), ruleParameter);
        }
        parameterLoadingCache.putAll(map);
        return ruleParameters;
    }

    @Override
    public void add(RuleParameter parameter) {
        persistentService.persist(parameter);
        parameterLoadingCache.put(parameter.getId(), parameter);
    }

    @Override
    public void remove(RuleParameter parameter) {
        persistentService.remove(parameter);
        parameterLoadingCache.invalidate(parameter.getId());
    }

    @Override
    public void update(RuleParameter parameter) {
        persistentService.merge(parameter);
        parameterLoadingCache.put(parameter.getId(), parameter);
    }

    @Override
    public RuleParameter get(Integer parameterId) throws RoutingException {
        try {
            return parameterLoadingCache.get(parameterId);
        } catch (ExecutionException e) {
            throw new RoutingException(RoutingExceptionCode.COMMON, e.getCause());
        }
    }

}
