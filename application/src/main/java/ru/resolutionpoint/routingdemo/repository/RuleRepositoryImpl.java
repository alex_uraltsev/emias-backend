package ru.resolutionpoint.routingdemo.repository;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import ru.resolutionpoint.routingdemo.entity.Rule;
import ru.resolutionpoint.routingdemo.exception.RoutingException;

import javax.ejb.Singleton;
import java.util.*;
import java.util.concurrent.ExecutionException;

@Singleton
public class RuleRepositoryImpl extends BaseRepository<Rule> implements RuleRepository {

    private LoadingCache<Integer, Rule> ruleCache =
            CacheBuilder.newBuilder()
                    .build(new CacheLoader<Integer, Rule>() { // build the cacheloader
                        @Override
                        public Rule load(Integer ruleId) throws Exception {
                            //make the expensive call
                            return getByIdFromDatabase(Rule.class, ruleId);
                        }
                    });

    @Override
    public Collection<Rule> all() throws RoutingException {
        if (ruleCache.size() > 0) {
            return ruleCache.asMap().values();
        }

        List<Rule> rules = getAllFromDatabase(Rule.class);
        Map<Integer, Rule> map = new HashMap<>();
        for (Rule rule : rules) {
            map.put(rule.getId(), rule);
        }
        ruleCache.putAll(map);
        return rules;
    }

    @Override
    public void add(Rule rule) {
        persistentService.persist(rule);
        ruleCache.put(rule.getId(), rule);
    }

    @Override
    public void remove(Rule rule) {
        persistentService.remove(rule);
        ruleCache.invalidate(rule.getId());
    }

    @Override
    public void update(Rule rule) {
        persistentService.merge(rule);
        ruleCache.put(rule.getId(), rule);
    }

    @Override
    public Rule get(Integer ruleId) throws RoutingException {
        try {
            return ruleCache.get(ruleId);
        } catch (ExecutionException e) {
            throw new RoutingException(e.getCause());
        }
    }
}
