package ru.resolutionpoint.routingdemo.repository;

import ru.resolutionpoint.routingdemo.services.PersistentService;

import javax.ejb.EJB;
import java.util.List;

public abstract class BaseRepository<T> {

    @EJB
    protected PersistentService<T> persistentService;

    protected T getByIdFromDatabase(Class<T> clazz, Integer ruleParameterId) {
        return persistentService.getById(clazz, ruleParameterId);
    }

    protected List<T> getAllFromDatabase(Class<T> clazz) {
        return persistentService.loadAll(clazz);
    }

}
